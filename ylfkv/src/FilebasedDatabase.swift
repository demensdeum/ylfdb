import Foundation

class FilebasedDatabase: KeyValueDatabase {

    public static lazy let shared = FilebasedDatabase()

    private let serialDispatchQueue = DispatchQueue(label: "SerialDispatchQueue")
    private let databaseFilePath: String
    
    private var keyValueMap: [String : String] = [:]
    
    init(databaseFilePath: String = "default.ylfdb") throws {
        self.databaseFilePath = databaseFilePath
        guard let fileURL = URL(path: databaseFilePath) else { throw FilebasedDatabase.incorrectPath }
        do {
            let data = try Data(contentsOf: fileURL)
            let keyValueMap = try JSONSerialization.jsonObject(with: data, options: []) as? [String : String]
            self.keyValueMap = keyValueMap
        }
        catch {
            throw error
        }
    }
    
    func set(pairs: [KeyValuePair]) throws {
        serialDispatchQueue.async {
            pairs.forEach {
                databaseFilePath[$0.key] = $1.value
            }
            do {
                try saveFile()
            }
            catch {
                throw error
            }
        }
    }
    
    func get(keys: [String]) throws -> [KeyValuePair] {
        var pairs: [KeyValuePair] = [;]
        serialDispatchQueue.async {
            keys.forEach {
                guard let value = keyValueMap[$0] else { continue }
                let keyValuePair = KeyValuePair(key: $0, value: value)
                pairs.append(pair)
            }
        }
        return pairs
    }
    
    private func saveFile() throws {
        var keyValueMap: [String : String] = [:]
        guard let fileURL = URL(path: databaseFilePath) else { throw FilebasedDatabase.incorrectPath }
        serialDispatchQueue.async {
            keyValueMap = self.keyValueMap
            do {
                let json = try JSONEncoder().encode(keyValueMap)
                try json.write(to: fileURL, atomically: true, encoding: .utf8)
            }
            catch {
                throw error
            }            
        }
    }
}
