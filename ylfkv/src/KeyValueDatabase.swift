import Foundation

protocol KeyValueDatabase {
    func set(pairs: [KeyValuePair]) throws
    func get(keys: [String]) throws -> [KeyValuePair]
    func remove(keys: [String]) throws
    func removeAll() throws
}
