 let db = FilebasedDatabase.shared
 db.removeAll()
 let pair = KeyValuePair(key: "TestKey", value: "TestValue")
 db.set(pairs: [pair])
 let dbPairs = db.get(keys: ["TestKey"])
 let savedPair = dbPairs.first
 if savedPair.key != pair.key || savedPair.value != pair.value {
    print("Test failed")
    exit(1)
 }
 exit(0)
